# Blake Newman - Challenge

## Introduction
I have started the task by setting up a base project set-up for each course. I typically use brunch for my development but I have recently familiarised myself with grunt, so I will continue to use this for both tasks as it is a better known JavaScript runner set-up; my gruntfile is in coffee purely for better readability. 

I am using scss for both projects, this is one of my favourite preprocessors along side stylus. I am usin bourbon and neat; however I have still to fully familiarise myself with the libraries to fully benefit using them. There may be some mixins that I could use that will enable better results across all browsers.

I have also used templates (handlebars) to keep HTML more modular. I will be using my own MVC framework that is in development. It is similar to backbone but more focused on modularising code into OOP formats with a CommonJS style. A massive benefit to it is that it makes use of components that really give an edge to reusable and extendible code; components also wrap MVC modules - which really make using MVC straight forward. Components will automatically look up its hierarchy; which really improves there extendibility. For more information please look at http://tweakjs.com for more info; please bare in mind that its new and still in early development stages so there is areas that are being improved on. I will be using v1.8.0 for this project. To support the CommonJS set-up I am using the commonjs-require-definition that brunch uses - its lightweight and a simple module loader. I know jQuery is not approved off however with modern day technology jQuery doesn't create any hindrance in real life scenarios. jQuery is depended on by tweak.js (zepto and other libraries are still to be fully tested); as its not advised to use jQuery I will not use it for anything other than to cover the dependencies.

I will use CSS/SCSS for any animations - however I do have a full understanding of GSAP which is great for higher level animations that require more control. 

Compatability: 9+, Chrome 20+, Firefox 10+. Compaatability may be better on Chrome and firefox but thats ar far back as I could test. 

## Task 1
Didnt run into any known problems. 


## Task 2
Couple of layout issues - was difficult to get all details from wireframe such as font size; as it was embedded into PDF. Couldnt work out a good way to get the 'more links' to wrap inside text when on desktop version. 

May look bland - but take a a look round the page and you will find some slick animations. The menu on mobile is quite playful and links have smooth animations.