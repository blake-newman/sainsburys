module.exports = (grunt) ->
  grunt.initConfig

    pkg: grunt.file.readJSON('package.json')  

    ###
      Assign variables for the grunt file
    ###
    project:
      app: '<%= _.slugify(appname) %>'

      scss: '<%= project.source.dir %>scss/app.scss'

      vendor:
        bower: './bower_components/'
        bourbon: '<%= project.vendor.bower %>bourbon/app/assets/stylesheets/'
        neat: '<%= project.vendor.bower %>neat/app/assets/stylesheets/'
        normalize: '<%= project.vendor.bower %>normalize.css/'

      source:
        dir: 'src/'
        app: '<%= project.source.dir %>scripts/app.js'
        scss: '<%= project.source.dir %>**/*.scss'
        css: '<%= project.source.dir %>**/*.css'
        hbs: '<%= project.source.dir %>**/*.hbs'
        js: '<%= project.source.dir %>**/*.js'
        coffee: '<%= project.source.dir %>**/*.coffee'
        static: '<%= project.source.dir %>**/static/**/*.*'

      build: 
        dir: 'build/'
        css: '<%= project.build.dir %>**/*.css'
        js: '<%= project.build.dir %>**/*.js'
        html: '<%= project.build.dir %>**/*.html'

      coffee: '<%= project.build.dir %>coffee.js'
      hbs: '<%= project.build.dir %>templates.js'

      style: '<%= project.build.dir %>/main.css'
      script: '<%= project.build.dir %>/main.js'

    ###
      Watch files and perform tasks upon changes
    ###
    watch:
      options:
        livereload: true

      sass:
        files: [
          '<%= project.source.scss %>'
          '<%= project.vendor.bower %>/**/*.scss'
          '!<%= project.source.static %>'
        ]
        tasks: [
          'styles:dev',
          'clean:extras'
        ]

      scripts:
        options:
          reload: true
        files: [
          '<%= project.vendor.bower %>/**/*.js'
          '<%= project.source.coffee %>'
          '<%= project.source.hbs %>'
          '<%= project.source.js %>'
          '!<%= project.source.static %>'
        ]
        tasks: [
          'scripts:dev'
          'clean:extras'
        ]

      # Static file listener - don't want to copy static files everytime there is a js/style update
      staticCopy: 
        files: [
          '<%= project.source.static %>'
        ]
        tasks: [
          'copy:staic'
        ]

      # Copy all other files that are not in static folder and not those that are compiled
      copy:
        files: [
          '<%= project.source.dir %>**'
          '!<%= project.source.static %>'
          '!<%= project.source.scss %>'
          '!<%= project.source.js %>'
          '!<%= project.source.coffee %>'
          '!<%= project.source.hbs %>'
        ]
        tasks: [
          'copy:build'
        ]

    ###
      Server stuff
    ###
    connect:
      server:
        options:
          port: 8888
          hostname: '0.0.0.0'
          base: '<%= project.build.dir %>'

    ###
      Compiles CoffeeScript files into a single js file in build directory
    ###
    coffee:
      default:
        options:
          join: true
          sourceMap: false
          bare: true
        
        files: 
          '<%= project.coffee %>': [
            '<%= project.source.coffee %>'
          ]

    ###
      Compiles handlebar files into a single js file in build directory
    ###
    handlebars:
      default:        
        files: 
          '<%= project.hbs %>': [
            '<%= project.source.hbs %>'
          ]
    
    ###
      JS oncatenation tasks. Unsure how to ges source maps to work with multiple concatination
    ###
    concat:
      default:
        options:
          sourceMap:false
        files: 
          '<%= project.script %>': [
            '<%= project.source.js %>'
            '!<%= project.source.app %>'
            '<%= project.source.app %>'
            '<%= project.coffee %>'
          ]

    ###
      JS minification tasks.
    ###
    uglify:
      default:
        options:
          sourceMap : false
          drop_console: true
          preserveComments: 'none'
        files: 
          '<%= project.script %>': [
            '<%= project.script %>'
          ]
      
    ###
      Sass based tasks and configuration
    ###
    sass:
      default:
        options:
          style: 'nested'
          precision: 5
          sourcemap: 'auto'
          loadPath: [
            '<%= project.vendor.bourbon %>'
            '<%= project.vendor.neat %>'
            '<%= project.vendor.normalize %>'
          ]
        files:
          '<%= project.style %>': [
            '<%= project.scss %>'
          ]
      
    ###
      CSS minification
    ###
    cssmin:
      default:
        target:
          files:
            '<%= project.style %>': [
              '<%= project.style %>'
            ]

    ###
      Image minification. Never apply compression to the source files as in some
      instances the files become over compressed; meaning you have no originals. Causing more work.
    ###
    imagemin:
      build:
        files: [{
          expand: true
          cwd: '<%= project.build.dir %>images'
          src: '{,*/}*.{gif,jpeg,jpg,png}'
          dest: '<%= project.build.dir %>images'
        }]

    ###
      Copy tasks
    ###
    copy:
      static:
        cwd: '<%= project.source.dir %>'
        src: [
          '**/static/**/*'
        ]
        dest: '<%= project.build.dir %>'
        expand: true
        filter:'isFile'

      build:
        cwd: '<%= project.source.dir %>'
        src: [
          '**'
          '!**/static/**/*'
          '!**/*.hbs'
          '!**/*.coffee'
          '!**/*.scss'
          '!**/*.css'
          '!**/*.js'
        ]
        dest: '<%= project.build.dir %>'
        expand: true
        filter:'isFile'

    ###
      Cleaning tasks for the environment
    ###
    clean:
      build:
        src: [
          '<%= project.build.dir %>'
        ]
      extras:
        src: [
          '<%= project.coffee %>'
          '<%= project.hbs %>'
        ]

    ###
      Bust the cache on files
    ###
    'cache-busting':
      script:
        replace: ['<%= project.build.dir %><%= project.html %>']
        replacement: 'main.js'
        file: '<%= project.script %>'
      
      style:
        replace: ['<%= project.build.dir %><%= project.html %>']
        replacement: 'main.css'
        file: '<%= project.style %>'
        cleanup: true


  ###
    Auto registers tasks to grunt
  ###
  require('load-grunt-tasks') grunt

  ###
    Time how long tasks take. Can help when optimizing build times
  ###
  require('time-grunt') grunt


  ###
    Tasks are categorised by their task type.

    scripts (dev & dist)
    styles (dev & dist)
    build - This is shared build tasks
    dist - This will do more expensive tasks that are needed prior to distribution
    default - default task (watches the environment for changes)
  ###

  grunt.registerTask 'scripts:dist', [
    'scripts:dev'
    'uglify'
  ]
     
  grunt.registerTask 'scripts:dev', [  
    'coffee'
    'handlebars'
    'concat'
  ]

  grunt.registerTask 'styles:dist', [
    'styles:dev'
    'cssmin'
  ]
     
  grunt.registerTask 'styles:dev', [
    'sass'
  ]
   
  grunt.registerTask 'build', [
    'clean:build'
    'copy:build'    
    'copy:static'
  ]

  grunt.registerTask 'dist', [
    'build'  
    'scripts:dist' 
    'styles:dist'
    'clean:extras'
  ]
    
  grunt.registerTask 'default', [
    'build'  
    'scripts:dev'
    'styles:dev'    
    'clean:extras'
    'connect'
    'watch'
  ]

  return