(function() {
  "use strict";

  require.register('flickr-service', function (exports, require, module){
    var FlickrService;

    module.exports = exports = FlickrService = (function(){
      
      function FlickrService() {};

      FlickrService.retrieve = function(tags, callback) {        
        var script, url;

        window.FlickrServiceCallback =  function(data){
          script.parentNode.removeChild(script);
          callback(data);
        };

        url = encodeURI('http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=FlickrServiceCallback&tags='+tags);        
        script = document.createElement('script');
        script.src = url;        
        document.head.appendChild(script);


      };

      return FlickrService

    })();
  });

})();

