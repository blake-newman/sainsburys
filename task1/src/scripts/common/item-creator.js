(function() {
  "use strict";

  require.register('item-creator', function (exports, require, module){
    var ItemCreator;

 
    module.exports = exports = ItemCreator = (function(){
      
      var component; 

      function ItemCreator() {};

      ItemCreator.create = function(data) {
        var items;

        // only 6 items at a time
        items = data.items;
        items = items.splice(items.length-6, 6);


        component = new Tweak.Component(window, {
          name: 'items',
          collection: items,
          view: {
            attach: 'items',
            method: 'replace',
            template: 'src/templates/items.hbs'
          }
        });

        component.config.view.data = component.collection;
        component.init();        
        component.render();

      };

      return ItemCreator

    })();
  });

  require.register('items/view', function (exports, require, module){
    var ItemsView;

 
    module.exports = exports = ItemsView = (function(){
      
      var component;

      Tweak['extends'](ItemsView, Tweak.View);

      function ItemsView(obj) {
        // Supers Tweak.view
        Tweak.super(ItemsView, this, 'constructor', obj);
      };

      
      ItemsView.prototype.ready = function(){
        var targs, targ, i, _length, cName;
        
        targs = this.el.querySelectorAll('.item-holder');
        
        _length = targs.length;
        i = 0;
        for (; i < _length; i++) {
          targ = targs[i];
          targ.addEventListener('click', function(){
            cName = this.className;
            if (cName.match(/\s*selected/)) {
              this.className = cName.replace(/\s*selected/, '');
            } else {
              this.className += ' selected';
            }
          });
        };
      }

      return ItemsView

    })();
  });



})();

