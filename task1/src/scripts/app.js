(function() {
  "use strict";

  var flickrService, itemCreator;


  require('jst-tweaked');
  
  flickrService = require('flickr-service');

  itemCreator = require('item-creator');

  flickrService.retrieve('London', itemCreator.create);
  

})();
